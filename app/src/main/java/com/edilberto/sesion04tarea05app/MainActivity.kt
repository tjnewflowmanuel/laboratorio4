package com.edilberto.sesion04tarea05app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),IListas {
    var vehiculoelegido = ""
    var contador = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tipovehiculo = listOf("Modelo1", "Modelo2","Modelo3","Modelo4","Modelo5","Modelo6")
        val imagenescarros = intArrayOf(R.drawable.carro1, R.drawable.carro2, R.drawable.carro3, R.drawable.carro4, R.drawable.carro5, R.drawable.carro6)

        //val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
        val adapter = ArrayAdapter(this, R.layout.style_spinner,tipovehiculo)
        spLenguajes.adapter = adapter

        spLenguajes.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //Toast.makeText(this@MainActivity, languages[position], Toast.LENGTH_SHORT).show()

                if (contador!=0){
                    createLoginDialogo(tipovehiculo[position]).show()
                }
                contador++

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        btnVerificar.setOnClickListener {

            if (vehiculoelegido.isEmpty()){
                Toast.makeText(this,"Debe elegir un vehiculo", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val nombrepropietario = edtNombres.text.toString()
            val aniovehiculo = edtDocumento.text.toString()
            val problemvehiculo = edtproblema.text.toString()

            val persona = Propietario(nombrepropietario,aniovehiculo,problemvehiculo,vehiculoelegido)

            tvResultado.text = "Estimado ${persona.nombre}. Su vehiculo del año ${persona.vehiculo}. Tiene este problema ${persona.problema}. realice el pago en ventanilla para la reparacion de su vehiculo " +
                    "identidad que tenemos registrado de su vehiculo es el modelo ${persona.documento}"
        }

    }

    override fun createLoginDialogo(tipovehiculo:String): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)
        builder.setView(v)
        val btnAperturarNo: Button = v.findViewById(R.id.btn_aperturar_no)
        val btnAperturarSi: Button = v.findViewById(R.id.txtitem)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)

        if (vehiculoelegido == "Modelo1"){
            imgLogo.setBackgroundResource(R.drawable.carro1)
        }
        if (vehiculoelegido=="Modelo2") {
            imgLogo.setBackgroundResource(R.drawable.carro2)
        }
        if (vehiculoelegido=="Modelo3") {
            imgLogo.setBackgroundResource(R.drawable.carro3)
        }
        if (vehiculoelegido=="Modelo4") {
            imgLogo.setBackgroundResource(R.drawable.carro4)
        }
        if (vehiculoelegido=="Modelo5") {
            imgLogo.setBackgroundResource(R.drawable.carro5)
        }
        if (vehiculoelegido=="Modelo6") {
            imgLogo.setBackgroundResource(R.drawable.carro6)
        }

        //crear pop up
        alertDialog = builder.create()

        btnAperturarSi.setOnClickListener {
            vehiculoelegido = tipovehiculo
            Toast.makeText(this,"Has seleccionado $tipovehiculo", Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnAperturarNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }


}
