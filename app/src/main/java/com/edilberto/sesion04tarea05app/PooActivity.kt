package com.edilberto.sesion04tarea05app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class PooActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p_o_o)

        val Carro = Carro("CarroGas",1994)

        Carro?.let {
            println(it.dormir())
            println(it.cierre())
        }

        val carroGas = CarroGas("MechanicCar")
        println(carroGas.encender())
        carroGas.Modelo = "CarroGas"
        carroGas.Anio = 1994
        println(carroGas.encender())
        println(carroGas.cierre())

        val carroMecanico = CarroMecanico(1)
        println(carroMecanico.maullar())


    }
}