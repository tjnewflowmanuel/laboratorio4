package com.edilberto.sesion04tarea05app

class CarroGas(val marca:String) : Carro(Modelo = "",Anio = 0) {

    fun encender(){
        println("El vehiculo de modelo $marca se aperturo un año despues")
    }

    override fun cierre() {
        println("El $Modelo que tiene $Anio años esta sin stock")
    }
}